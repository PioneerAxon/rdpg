#include<stdlib.h>
#include<stdio.h>
#include<malloc.h>

#ifndef __NFA__
	#define __NFA__

#define NFA_STATE_INITIAL 1
#define NFA_STATE_FINAL 2
#define NFA_STATE_NORMAL 0

struct _NFA_State
{
	int id;
	int type;
};

typedef struct _NFA_State NFA_State;

struct _NFA_Symbol
{
	int id;
	char symbol;
};

typedef struct _NFA_Symbol NFA_Symbol;

struct _NFA_Transition
{
	int id;
	int from;
	int on;
	int* to;
	int to_count;
};

typedef struct _NFA_Transition NFA_Transition;

struct _NFA
{
	int id;
	
	int _state_id;//0
	int _symbol_id;//0
	int _transition_id;//0
	int _initial_state_id;//-1
	
	NFA_Symbol * _symbol_pool;//null
	NFA_State * _state_pool;//null
	NFA_Transition * _transition_pool;//null

	int _symbol_pool_count;//0
	int _state_pool_count;//0
	int _transition_pool_count;//0
};

typedef struct _NFA NFA;

int _nfa_id = 0;
NFA * _nfa_pool = NULL;
int _nfa_pool_count = 0;

int nfa_add_symbol (NFA * nfa, char c)
{
	if (nfa->_symbol_pool == NULL)
	{
		nfa->_symbol_pool = (NFA_Symbol *) malloc (sizeof (NFA_Symbol));
		nfa->_symbol_pool_count = 0;
	}
	else
	{
		nfa->_symbol_pool = (NFA_Symbol *) realloc (nfa->_symbol_pool, (nfa->_symbol_pool_count + 1) * sizeof (NFA_Symbol));
	}
	nfa->_symbol_pool [nfa->_symbol_pool_count].id = nfa->_symbol_id;
	nfa->_symbol_pool [nfa->_symbol_pool_count].symbol = c;
	nfa->_symbol_pool_count++;
	return (nfa->_symbol_id++);
}

int nfa_check_symbol_exists (NFA * nfa, char c)
{
	int l;
	for (l = 0; l < nfa->_symbol_pool_count; l++)
	{
		if (nfa->_symbol_pool [l].symbol == c)
		{
			return nfa->_symbol_pool [l].id;
		}
	}
	return -1;
}

int nfa_check_create_symbol (NFA * nfa, char c)
{
	int tmp;
	if ((tmp = nfa_check_symbol_exists (nfa, c)) == -1)
	{
		return nfa_add_symbol (nfa, c);
	}
	else
	{
		return tmp;
	}
}

int nfa_add_state (NFA * nfa)
{
	if (nfa->_state_pool == NULL)
	{
		nfa->_state_pool = (NFA_State *) malloc (sizeof (NFA_State));
		nfa->_state_pool_count = 0;
	}
	else
	{
		nfa->_state_pool = (NFA_State *) realloc (nfa->_state_pool, (nfa->_state_pool_count + 1) * sizeof (NFA_State));
	}
	nfa->_state_pool [nfa->_state_pool_count].id = nfa->_state_id;
	nfa->_state_pool [nfa->_state_pool_count].type = NFA_STATE_NORMAL;
	nfa->_state_pool_count++;
	return (nfa->_state_id++);
}

int nfa_check_state_exists (NFA * nfa, int id)
{
	return (id < nfa->_state_id);
}

void nfa_check_create_state (NFA * nfa, int desired_id)
{
	if (! nfa_check_state_exists (nfa, desired_id))
	{
		while (desired_id != nfa_add_state (nfa));
	}
}

void nfa_add_special_state_type (NFA * nfa, int state_id, int type)
{
	nfa_check_create_state (nfa, state_id);
	nfa->_state_pool [state_id].type = type;
	if (type == NFA_STATE_INITIAL)
	{
		nfa->_initial_state_id = state_id;
	}
}

int nfa_check_similar (NFA * nfa, int from_state_id, int on_symbol_id)
{
	int l;
	for (l = 0; l < nfa->_transition_pool_count; l++)
	{
		if (nfa->_transition_pool [l].from == from_state_id && on_symbol_id == nfa->_transition_pool [l].on)
			return nfa->_transition_pool [l].id;
	}
	return -1;
}

int nfa_add_transition (NFA * nfa, int from_state_id, int on_symbol_id, int to_state_id)
{
	int tmp;
	nfa_check_create_state (nfa, from_state_id);
	nfa_check_create_state (nfa, to_state_id);
	if ((tmp = nfa_check_similar (nfa, from_state_id, on_symbol_id)) == -1)
	{
		if (nfa->_transition_pool == NULL)
		{
			nfa->_transition_pool = (NFA_Transition *) malloc (sizeof (NFA_Transition));
			nfa->_transition_pool_count = 0;
		}
		else
		{
			nfa->_transition_pool = (NFA_Transition *) realloc (nfa->_transition_pool, (nfa->_transition_pool_count + 1) * sizeof (NFA_Transition));
		}
		nfa->_transition_pool [nfa->_transition_pool_count].id = nfa->_transition_id;
		nfa->_transition_pool [nfa->_transition_pool_count].from = from_state_id;
		nfa->_transition_pool [nfa->_transition_pool_count].to_count = 0;
		nfa->_transition_pool [nfa->_transition_pool_count].to = NULL;
		nfa->_transition_pool [nfa->_transition_pool_count].on = on_symbol_id;
		tmp = nfa->_transition_pool_count;
		nfa->_transition_pool_count++;
	}
	nfa->_transition_pool [tmp].to = (int *) realloc (nfa->_transition_pool [tmp].to, (nfa->_transition_pool [tmp].to_count + 1) * sizeof (int));
	nfa->_transition_pool [tmp].to [nfa->_transition_pool [tmp].to_count] = to_state_id;
	nfa->_transition_pool [tmp].to_count++;
	return (nfa->_transition_id++);
}

int nfa_add_transition_from_symbol (NFA * nfa, int from_state_id, char on_symbol, int to_state_id)
{
	nfa_add_transition (nfa, from_state_id, nfa_check_create_symbol (nfa, on_symbol), to_state_id);
}

void nfa_print_all (NFA * nfa, FILE * str)
/* Just to show all info about the DFA. Useful only for debugging.. */
{
	int l,l1;
	fprintf (str, "Symbols in NFA:\n");
	for (l = 0; l < nfa->_symbol_pool_count; l++)
	{
		fprintf (str, "   Symbol : %d , %c \n", nfa->_symbol_pool [l].id, nfa->_symbol_pool [l].symbol); 
	}
	fprintf (str, "States in NFA:\n");
	for (l = 0; l < nfa->_state_pool_count; l++)
	{
		if (nfa->_state_pool [l].type == NFA_STATE_FINAL)
			fprintf (str, " * ");
		else if (nfa->_state_pool [l].type == NFA_STATE_INITIAL)
			fprintf (str, " + ");
		else
			fprintf (str, "   ");
		fprintf (str, "State : %d \n", nfa->_state_pool [l].id);
	}
	fprintf (str, "Transitions in NFA:\n");
	for (l = 0; l < nfa->_transition_pool_count; l++)
	{
		fprintf (str, "   Transition : %d , (%d, %c) -> {", nfa->_state_pool [nfa->_transition_pool [l].from].id, nfa->_symbol_pool [nfa->_transition_pool [l].on].id, nfa->_symbol_pool [nfa->_transition_pool [l].on].symbol);
		for (l1 = 0; l1 < nfa->_transition_pool [l].to_count; l1++)
			fprintf(str, " %d",nfa->_transition_pool [l].to [l1]);
		fprintf(str, " }\n");
	}
}

int nfa_add_nfa ()
{
	if (_nfa_pool == NULL)
	{
		_nfa_pool = (NFA *) malloc (sizeof (NFA));
		_nfa_pool_count = 0;
	}
	else
	{
		_nfa_pool = (NFA *) realloc (_nfa_pool, (_nfa_pool_count + 1) * sizeof (NFA));
	}
	_nfa_pool [_nfa_pool_count].id = _nfa_id++;
	_nfa_pool [_nfa_pool_count]._initial_state_id = -1;
	_nfa_pool [_nfa_pool_count]._state_id = 0;
	_nfa_pool [_nfa_pool_count]._symbol_id = 0;
	_nfa_pool [_nfa_pool_count]._transition_id = 0;
	_nfa_pool [_nfa_pool_count]._state_pool = NULL;
	_nfa_pool [_nfa_pool_count]._symbol_pool = NULL;
	_nfa_pool [_nfa_pool_count]._transition_pool = NULL;
	_nfa_pool [_nfa_pool_count]._state_pool_count = 0;
	_nfa_pool [_nfa_pool_count]._symbol_pool_count = 0;
	_nfa_pool [_nfa_pool_count]._transition_pool_count = 0;
	_nfa_pool_count++;
	return (_nfa_id -1);
}

#endif
