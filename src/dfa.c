#include<stdlib.h>
#include<stdio.h>
#include<malloc.h>

#ifndef __DFA__
	#define __DFA__

#define DFA_STATE_INITIAL 1
#define DFA_STATE_FINAL 2
#define DFA_STATE_NORMAL 0

struct _DFA_State
{
	int id;
	int type;
};

typedef struct _DFA_State DFA_State;

struct _DFA_Symbol
{
	int id;
	char symbol;
};

typedef struct _DFA_Symbol DFA_Symbol;

struct _DFA_Transition
{
	int id;
	int from;
	int on;
	int to;
};

typedef struct _DFA_Transition DFA_Transition;

struct _DFA
{
	int id;
	
	int _state_id;//0
	int _symbol_id;//0
	int _transition_id;//0
	int _initial_state_id;//-1
	
	DFA_Symbol * _symbol_pool;//null
	DFA_State * _state_pool;//null
	DFA_Transition * _transition_pool;//null

	int _symbol_pool_count;//0
	int _state_pool_count;//0
	int _transition_pool_count;//0
};

typedef struct _DFA DFA;

int _dfa_id = 0;
DFA * _dfa_pool = NULL;
int _dfa_pool_count = 0;


int dfa_add_symbol (DFA * dfa, char c)
{
	if (dfa->_symbol_pool == NULL)
	{
		dfa->_symbol_pool = (DFA_Symbol *) malloc (sizeof (DFA_Symbol));
		dfa->_symbol_pool_count = 0;
	}
	else
	{
		dfa->_symbol_pool = (DFA_Symbol *) realloc (dfa->_symbol_pool, (dfa->_symbol_pool_count + 1) * sizeof (DFA_Symbol));
	}
	dfa->_symbol_pool [dfa->_symbol_pool_count].id = dfa->_symbol_id;
	dfa->_symbol_pool [dfa->_symbol_pool_count].symbol = c;
	dfa->_symbol_pool_count++;	
	return (dfa->_symbol_id++);
}

int dfa_check_symbol_exists (DFA * dfa, char c)
{
	int l;
	for (l = 0; l < dfa->_symbol_pool_count; l++)
	{
		if (dfa->_symbol_pool [l].symbol == c)
		{
			return dfa->_symbol_pool [l].id;
		}
	}
	return -1;
}

int dfa_check_create_symbol (DFA * dfa, char c)
{
	int tmp;
	if ( (tmp = dfa_check_symbol_exists (dfa, c)) == -1)
	{
		return dfa_add_symbol (dfa, c);
	}
	else
	{
		return tmp;
	}
}

int dfa_add_state (DFA * dfa)
{
	if (dfa->_state_pool == NULL)
	{
		dfa->_state_pool = (DFA_State *) malloc (sizeof (DFA_State));
		dfa->_state_pool_count = 0;
	}
	else
	{
		dfa->_state_pool = (DFA_State *) realloc (dfa->_state_pool, (dfa->_state_pool_count + 1) * sizeof (DFA_State));
	}
	dfa->_state_pool [dfa->_state_pool_count].id = dfa->_state_id;
	dfa->_state_pool [dfa->_state_pool_count].type = DFA_STATE_NORMAL;
	dfa->_state_pool_count++;
	return (dfa->_state_id++);
}

int dfa_check_state_exists (DFA * dfa, int id)
{
	return (id < dfa->_state_id);
}

void dfa_check_create_state (DFA * dfa, int desired_id)
{
	if (! dfa_check_state_exists (dfa, desired_id))
	{
		while (desired_id != dfa_add_state (dfa));
	}
}

void dfa_add_special_state_type (DFA * dfa, int state_id, int type)
{
	dfa_check_create_state (dfa, state_id);
	dfa->_state_pool [state_id].type = type;
	if (type == DFA_STATE_INITIAL)
	{
		dfa->_initial_state_id = state_id;
	}
}

int dfa_add_transition (DFA * dfa, int from_state_id, int on_symbol_id, int to_state_id)
{
	dfa_check_create_state (dfa, from_state_id);
	dfa_check_create_state (dfa, to_state_id);
	if (dfa->_transition_pool == NULL)
	{
		dfa->_transition_pool = (DFA_Transition *) malloc (sizeof (DFA_Transition));
		dfa->_transition_pool_count = 0;
	}
	else
	{
		dfa->_transition_pool = (DFA_Transition *) realloc (dfa->_transition_pool, (dfa->_transition_pool_count + 1) * sizeof (DFA_Transition));
	}
	dfa->_transition_pool [dfa->_transition_pool_count].id = dfa->_transition_id;
	dfa->_transition_pool [dfa->_transition_pool_count].from = from_state_id;
	dfa->_transition_pool [dfa->_transition_pool_count].to = to_state_id;
	dfa->_transition_pool [dfa->_transition_pool_count].on = on_symbol_id;
	dfa->_transition_pool_count++;
	return (dfa->_transition_id++);
}

int dfa_add_transition_from_symbol (DFA * dfa, int from_state_id, char on_symbol, int to_state_id)
{
	dfa_add_transition (dfa, from_state_id, dfa_check_create_symbol (dfa, on_symbol), to_state_id);
}

void dfa_print_all (DFA * dfa, FILE * str)
/* Just to show all info about the DFA. Useful only for debugging.. */
{
	int l;
	fprintf (str, "Symbols in DFA:\n");
	for (l = 0; l < dfa->_symbol_pool_count; l++)
	{
		fprintf (str, "   Symbol : %d , %c \n", dfa->_symbol_pool [l].id, dfa->_symbol_pool [l].symbol); 
	}
	fprintf (str, "States in DFA:\n");
	for (l = 0; l < dfa->_state_pool_count; l++)
	{
		if (dfa->_state_pool [l].type == DFA_STATE_FINAL)	
			fprintf (str, " * ");
		else if (dfa->_state_pool [l].type == DFA_STATE_INITIAL)
			fprintf (str, " + ");
		else
			fprintf (str, "   ");
		fprintf (str, "State : %d \n", dfa->_state_pool [l].id);
	}
	fprintf (str, "Transitions in DFA:\n");
	for (l = 0; l < dfa->_transition_pool_count; l++)
	{
		fprintf (str, "   Transition : %d , (%d, %c) -> %d \n", dfa->_state_pool [dfa->_transition_pool [l].from].id, dfa->_symbol_pool [dfa->_transition_pool [l].on].id, dfa->_symbol_pool [dfa->_transition_pool [l].on].symbol, dfa->_state_pool [dfa->_transition_pool [l].to].id);
	}
}

int dfa_add_dfa ()
{
	if (_dfa_pool == NULL)
	{
		_dfa_pool = (DFA *) malloc (sizeof (DFA));
		_dfa_pool_count = 0;
	}
	else
	{
		_dfa_pool = (DFA *) realloc (_dfa_pool, (_dfa_pool_count + 1) * sizeof (DFA));
	}
	_dfa_pool [_dfa_pool_count].id = _dfa_id++;
	_dfa_pool [_dfa_pool_count]._initial_state_id = -1;
	_dfa_pool [_dfa_pool_count]._state_id = 0;
	_dfa_pool [_dfa_pool_count]._symbol_id = 0;
	_dfa_pool [_dfa_pool_count]._transition_id = 0;
	_dfa_pool [_dfa_pool_count]._state_pool = NULL;
	_dfa_pool [_dfa_pool_count]._symbol_pool = NULL;
	_dfa_pool [_dfa_pool_count]._transition_pool = NULL;
	_dfa_pool [_dfa_pool_count]._state_pool_count = 0;
	_dfa_pool [_dfa_pool_count]._symbol_pool_count = 0;
	_dfa_pool [_dfa_pool_count]._transition_pool_count = 0;
	_dfa_pool_count++;
	return (_dfa_id -1);
}

#endif
