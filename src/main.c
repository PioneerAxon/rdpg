#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include"dfa.c"
#include"nfa.c"
#include"nfa-dfa.c"
#include"enfa.c"
#include"enfa-nfa.c"
#include"filewriter.c"

char to_special (char inp)
//converts user escape char to actual escape char
{
	switch (inp)
	{
		case '\\' : return '\\';
		case '0' : return '\0';
		case 'n' : return '\n';
		case 't' : return '\t';
		case '$' : return '$';
		case '[' : return '[';
		case ']' : return ']';
		case 'b' : return ' ';
		default : return '\0';
	}
}

void get_array (FILE * in, char * c1, char * c2)
{
	int tmp;
	fscanf (in, "%c", c1);
	if (*c1 == '\\')
	{
		fscanf (in, "%c", c2);
		*c1 = to_special (*c2);
	}
	else if (*c1 == '$')
	{
		fscanf (in, "%d", &tmp);
		*c1 = (char) tmp;
	}
	fscanf (in, "-%c", c2);
	if (*c2 == '\\')
	{
		fscanf (in, "%c", c2);
		*c2 = to_special (*c2);
	}
	else if (*c2 == '$')
	{
		fscanf (in, "%d", &tmp);
		*c2 = (char) tmp;
	}
}

int get_insert_into_enfa (ENFA * enfa, FILE * in)
//get character to transit on
{
	char c1, c2;
	int tmp;
	int from, to;
	if (fscanf (in, "%d", &from) != EOF)
	{
		if (from < 0)
			return 0;
		fscanf (in, " %c", &c1);
		if (c1 == '[')
		{
			get_array (in, &c1, &c2);
			fscanf (in, "] %d", &to);
			while (c1 <= c2)
			{
				enfa_add_transition_from_symbol (enfa, from, c1, to);
				c1++;
			}
			return 1;
		}
		else if (c1 == '\\')
		{
			fscanf (in, "%c", &c2);
			if (c2 == 'N')
			{
				fscanf (in, "%d", &to);
				enfa_add_transition (enfa, from, 0, to);
				return 1;
			}
			c1 = to_special (c2);
		}
		else if (c1 == '$')
		{
			fscanf (in, "%d", &tmp);
			c1 = (char) tmp;
		}
		fscanf (in, "%d", &to);
		enfa_add_transition_from_symbol (enfa, from, c1, to);
		return 1;
	}
	return 0;
}

int main ()
{
	FILE* in = stdin;
	char c;
	int a,b;
	int enfa;
	int nfa;
	int dfa;
	enfa = enfa_add_enfa();
	nfa = nfa_add_nfa();
	dfa = dfa_add_dfa();
	fprintf(stderr, "Enter all transitions in form of <FROM> <ON> <TO> (CTRL+D to end)\n");
	while (get_insert_into_enfa (&_enfa_pool [enfa] , in) == 1) fflush (stdin);
	fprintf(stderr, "Enter start state : ");
	scanf ("%d",&b);
	enfa_add_special_state_type (&_enfa_pool [enfa], b, ENFA_STATE_INITIAL);
	fprintf(stderr, "Enter end states (CTRL+D to end)\n");
	while (fscanf (in, "%d", &a) != EOF)
	{
		if (a < 0)
			break;
		enfa_add_special_state_type (&_enfa_pool [enfa], a, ENFA_STATE_FINAL);
	}
	enfa_print_all (&_enfa_pool [enfa], stderr);
	enfa_nfa_convert (&_enfa_pool [enfa], &_nfa_pool [nfa]);
	nfa_print_all (&_nfa_pool [nfa], stderr);
	nfa_dfa_convert (&_nfa_pool [nfa], &_dfa_pool [dfa]);
	dfa_print_all (&_dfa_pool [dfa], stderr);
	file_print_all(stdout, &_dfa_pool [dfa]);
	exit (0);
}

