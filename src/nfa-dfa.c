#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<assert.h>
#include"dfa.c"
#include"nfa.c"

#ifndef __NFA_DFA__
#define __NFA_DFA__

struct _State_LL
{
	int id;
	struct _State_LL * next;
};

typedef struct _State_LL State_LL;

struct _State_Index
{
	int count;
	struct _State_LL * ll;
};

typedef struct _State_Index State_Index;

State_Index * _states_pool = NULL;
int _states_pool_count = 0;

State_LL * _tmp_list = NULL;
int _tmp_list_count = 0;

void nfa_dfa_copy_symbols (NFA * nfa, DFA * dfa)
{
	int l;
	for (l = 0; l < nfa->_symbol_pool_count; l++)
	{
		dfa_add_symbol (dfa, nfa->_symbol_pool [l].symbol);
	}
}


void nfa_dfa_cnvrt_print_tmp (FILE * str)
{
	State_LL * tmp;
	fprintf(str, "tmp = ");
	tmp = _tmp_list;
	while (tmp != NULL)
	{
		fprintf(str, "%d ", tmp->id);
		tmp = tmp->next;
	}
	fprintf(str, "\n");
}

void nfa_dfa_cnvrt_print_all(FILE * str)
{
	int l;
	State_LL * tmp;
	fprintf (str, "NFA to DFA conversion chart : \n");
	for (l = 0; l < _states_pool_count;l++)
	{
		fprintf(str, "   DFA id = %d, NFA id = { ", l, _states_pool [l].count);
		tmp = _states_pool [l].ll;
		while (tmp != NULL)
		{
			fprintf(str, "%d ", tmp->id);
			tmp = tmp->next;
		}
		fprintf(str, "}\n");
	}
}

int nfa_dfa_insert_ll ()
{
	if (_states_pool == NULL)
	{
		_states_pool = (State_Index *) malloc ((_states_pool_count + 1) * sizeof (State_Index));
		_states_pool_count = 0;
	}
	else
	{
		_states_pool = (State_Index *) realloc (_states_pool, (_states_pool_count + 1) * sizeof (State_Index));
	}
	_states_pool [_states_pool_count].count = 0;
	_states_pool [_states_pool_count].ll = NULL;
	return _states_pool_count++;
}

void nfa_dfa_insert_into_tmp (int val)
{
	State_LL * ptr;
	State_LL * new;
	new = (State_LL *) malloc (sizeof (State_LL));
	new->id = val;
	new->next = NULL;
	if (_tmp_list == NULL)
	{
		_tmp_list = new;
		_tmp_list_count = 0;
	}
	else
	{
		ptr = _tmp_list;
		if (ptr->id == val)
		{
			free (new);
			return;
		}
		if (ptr->id > val)
		{
			new->next = ptr;
			_tmp_list = new;
			_tmp_list_count++;
			return;
		}
		while (ptr->next != NULL)
		{
			if (ptr->next->id == val)
			{
				free (new);
				return;
			}
			if (ptr->next->id > val)
			{
				break;
			}
			ptr=ptr->next;
		}
		new->next = ptr->next;
		ptr->next = new;
	}
	_tmp_list_count++;
}

void nfa_dfa_insert_into_ll (int index, int val)
{
	State_LL * ptr;
	State_LL * new;
	if (index > _states_pool_count)
	{
		while (index != nfa_dfa_insert_ll ());
	}
	new = (State_LL *) malloc (sizeof (State_LL));
	new->id = val;
	new->next = NULL;
	if (_states_pool [index].ll == NULL)
	{
		_states_pool [index].ll = new;
	}
	else
	{
		ptr = _states_pool [index].ll;
		if (ptr->id == val)
		{
			free (new);
			return;
		}
		if (ptr->id > val)
		{
			new->next = ptr;
			_states_pool [index].ll = new;
			_states_pool [index].count++;
			return;
		}
		while (ptr->next != NULL)
		{
			if (ptr->next->id == val)
			{
				free (new);
				return;
			}
			if (ptr->next->id > val)
			{
				break;
			}
			ptr = ptr->next;
		}
		new->next = ptr->next;
		ptr->next = new;
	}
	_states_pool [index].count++;
}

int nfa_dfa_compare (State_LL * from1, State_LL * from2)
{
	while ( from1 != NULL && from2 != NULL)
	{
		if (from1->id != from2->id)
			return 0;
		from1 = from1->next;
		from2 = from2->next;
	}
	if (from1 == NULL && from2 == NULL)
		return 1;
	return 0;
}

int nfa_dfa_compare_add_state ()
{
	int l;
	int tmp = _states_pool_count;
	int new_ll;
	State_LL *prev;
	State_LL *next;
	for (l = 0; l < _states_pool_count; l++)
	{
		if (_states_pool [l].count == _tmp_list_count)
		{
			if (nfa_dfa_compare (_tmp_list, _states_pool [l].ll) == 1)
			{
				prev = _tmp_list;
				while (prev != NULL)
				{
					if ( prev->next == NULL)
					{
						free (prev);
						break;
					}
					next = prev->next;
					free (prev);
					prev = next;
				}
				_tmp_list = NULL;
				_tmp_list_count = 0;
				return l;
			}
		}
	}
	new_ll = nfa_dfa_insert_ll ();
	_states_pool [new_ll].ll = _tmp_list;
	_states_pool [new_ll].count = _tmp_list_count;
	_tmp_list = NULL;
	_tmp_list_count = 0;
	return (_states_pool_count - 1);
}


int nfa_dfa_process_transition_from_states_on_symbol (NFA * nfa, int convert_from_id, int on_symbol)
{
	int l1, l2, l3;
	int from_id;
	State_LL * ptr;
	ptr = _states_pool [convert_from_id].ll;
	for (l1 = 0; l1 < _states_pool [convert_from_id].count ;l1++)
	{
		for (l2 =0; l2 < nfa->_transition_pool_count; l2++)
		{
			if (nfa->_transition_pool [l2].from == ptr->id && nfa->_transition_pool [l2].on == on_symbol)
			{
				for (l3 = 0; l3 < nfa->_transition_pool [l2].to_count; l3++)
				{
					nfa_dfa_insert_into_tmp (nfa->_transition_pool [l2].to [l3]);
				}
			}
		}
		ptr = ptr->next;
	}
	return (nfa_dfa_compare_add_state ());
}

int nfa_dfa_check_is_final (NFA * nfa, int states_id)
{
	int l;
	State_LL * tmp;
	tmp = _states_pool [states_id].ll;
	for (l = 0; l < _states_pool [states_id].count; l++)
	{
		if (nfa->_state_pool [tmp->id].type == NFA_STATE_FINAL)
			return 1;
		tmp = tmp->next;
	}
	return 0;
}

void nfa_dfa_clear_ll (State_LL * from)
{
	State_LL * next;
	if (from == NULL)
		return;
	while (from != NULL)
	{
		next = from->next;
		free (from);
		from = next;
	}
}

void nfa_dfa_clear_all ()
{
	int l;
	if (_tmp_list != NULL)
	{
		nfa_dfa_clear_ll (_tmp_list);
		_tmp_list = 0;
		_tmp_list_count = 0;
	}
	for (l = 0; l < _states_pool_count; l++)
	{
		nfa_dfa_clear_ll (_states_pool [l].ll);
	}
	_states_pool = (State_Index *) realloc (_states_pool, 0);
	_states_pool_count = 0;
	_states_pool = NULL;
}

void nfa_dfa_convert (NFA * from, DFA * to)
{
	nfa_dfa_copy_symbols (from, to);
	int l, l1, l2;
	// Insert Empty state.
	nfa_dfa_insert_ll ();
	// Point empty state for each symbols
	for (l = 0; l < to->_symbol_pool_count; l++)
	{
		dfa_add_transition (to, 0, l, 0);
	}
	// Insert initial_state.
	nfa_dfa_insert_into_tmp (from->_initial_state_id);
	nfa_dfa_compare_add_state();
	for (l1 = 1; l1 < _states_pool_count; l1++)
	{
		for (l2 = 0; l2 < to->_symbol_pool_count; l2++)
		{
			dfa_add_transition (to, l1, l2, nfa_dfa_process_transition_from_states_on_symbol (from, l1, l2));
		}
	}
	dfa_add_special_state_type (to, 1, DFA_STATE_INITIAL);
	for (l1 = 0; l1 < _states_pool_count; l1++)
	{
		if (nfa_dfa_check_is_final (from, l1))
		{
			dfa_add_special_state_type (to, l1, DFA_STATE_FINAL);
		}
	}
	nfa_dfa_cnvrt_print_all (stderr);
	nfa_dfa_clear_all();
}

#endif
