#include<stdlib.h>
#include<stdio.h>
#include<malloc.h>

#ifndef __ENFA__
	#define __ENFA__

#define ENFA_STATE_INITIAL 1
#define ENFA_STATE_FINAL 2
#define ENFA_STATE_NORMAL 0

struct _ENFA_State
{
	int id;
	int type;
};

typedef struct _ENFA_State ENFA_State;

struct _ENFA_Symbol
{
	int id;
	char symbol;
};

typedef struct _ENFA_Symbol ENFA_Symbol;

struct _ENFA_Transition
{
	int id;
	int from;
	int on;
	int* to;
	int to_count;
};

typedef struct _ENFA_Transition ENFA_Transition;

struct _ENFA
{
	int id;
	
	int _state_id;//0
	int _symbol_id;//0
	int _transition_id;//0
	int _initial_state_id;//-1
	
	ENFA_Symbol * _symbol_pool;//null
	ENFA_State * _state_pool;//null
	ENFA_Transition * _transition_pool;//null

	int _symbol_pool_count;//0
	int _state_pool_count;//0
	int _transition_pool_count;//0
};

typedef struct _ENFA ENFA;

int _enfa_id = 0;
ENFA * _enfa_pool = NULL;
int _enfa_pool_count = 0;

int enfa_add_symbol (ENFA * enfa, char c)
{
	if (enfa->_symbol_pool == NULL)
	{
		enfa->_symbol_pool = (ENFA_Symbol *) malloc (sizeof (ENFA_Symbol));
		enfa->_symbol_pool_count = 0;
	}
	else
	{
		enfa->_symbol_pool = (ENFA_Symbol *) realloc (enfa->_symbol_pool, (enfa->_symbol_pool_count + 1) * sizeof (ENFA_Symbol));
	}
	enfa->_symbol_pool [enfa->_symbol_pool_count].id = enfa->_symbol_id;
	enfa->_symbol_pool [enfa->_symbol_pool_count].symbol = c;
	enfa->_symbol_pool_count++;
	return (enfa->_symbol_id++);
}

int enfa_check_symbol_exists (ENFA * enfa, char c)
{
	int l;
	for (l = 1; l < enfa->_symbol_pool_count; l++)
	{
		if (enfa->_symbol_pool [l].symbol == c)
		{
			return enfa->_symbol_pool [l].id;
		}
	}
	return -1;
}

int enfa_check_create_symbol (ENFA * enfa, char c)
{
	int tmp;
	if ((tmp = enfa_check_symbol_exists (enfa, c)) == -1)
	{
		return enfa_add_symbol (enfa, c);
	}
	else
	{
		return tmp;
	}
}

int enfa_add_state (ENFA * enfa)
{
	if (enfa->_state_pool == NULL)
	{
		enfa->_state_pool = (ENFA_State *) malloc (sizeof (ENFA_State));
		enfa->_state_pool_count = 0;
	}
	else
	{
		enfa->_state_pool = (ENFA_State *) realloc (enfa->_state_pool, (enfa->_state_pool_count + 1) * sizeof (ENFA_State));
	}
	enfa->_state_pool [enfa->_state_pool_count].id = enfa->_state_id;
	enfa->_state_pool [enfa->_state_pool_count].type = ENFA_STATE_NORMAL;
	enfa->_state_pool_count++;
	return (enfa->_state_id++);
}

int enfa_check_state_exists (ENFA * enfa, int id)
{
	return (id < enfa->_state_id);
}

void enfa_check_create_state (ENFA * enfa, int desired_id)
{
	if (! enfa_check_state_exists (enfa, desired_id))
	{
		while (desired_id != enfa_add_state (enfa));
	}
}

void enfa_add_special_state_type (ENFA * enfa, int state_id, int type)
{
	enfa_check_create_state (enfa, state_id);
	enfa->_state_pool [state_id].type = type;
	if (type == ENFA_STATE_INITIAL)
	{
		enfa->_initial_state_id = state_id;
	}
}

int enfa_check_similar (ENFA * enfa, int from_state_id, int on_symbol_id)
{
	int l;
	for (l = 0; l < enfa->_transition_pool_count; l++)
	{
		if (enfa->_transition_pool [l].from == from_state_id && on_symbol_id == enfa->_transition_pool [l].on)
			return enfa->_transition_pool [l].id;
	}
	return -1;
}

int enfa_add_transition (ENFA * enfa, int from_state_id, int on_symbol_id, int to_state_id)
{
	int tmp;
	enfa_check_create_state (enfa, from_state_id);
	enfa_check_create_state (enfa, to_state_id);
	if ((tmp = enfa_check_similar (enfa, from_state_id, on_symbol_id)) == -1)
	{
		if (enfa->_transition_pool == NULL)
		{
			enfa->_transition_pool = (ENFA_Transition *) malloc (sizeof (ENFA_Transition));
			enfa->_transition_pool_count = 0;
		}
		else
		{
			enfa->_transition_pool = (ENFA_Transition *) realloc (enfa->_transition_pool, (enfa->_transition_pool_count + 1) * sizeof (ENFA_Transition));
		}
		enfa->_transition_pool [enfa->_transition_pool_count].id = enfa->_transition_id;
		enfa->_transition_pool [enfa->_transition_pool_count].from = from_state_id;
		enfa->_transition_pool [enfa->_transition_pool_count].to_count = 0;
		enfa->_transition_pool [enfa->_transition_pool_count].to = NULL;
		enfa->_transition_pool [enfa->_transition_pool_count].on = on_symbol_id;
		tmp = enfa->_transition_pool_count;
		enfa->_transition_pool_count++;
	}
	enfa->_transition_pool [tmp].to = (int *) realloc (enfa->_transition_pool [tmp].to, (enfa->_transition_pool [tmp].to_count + 1) * sizeof (int));
	enfa->_transition_pool [tmp].to [enfa->_transition_pool [tmp].to_count] = to_state_id;
	enfa->_transition_pool [tmp].to_count++;
	return (enfa->_transition_id++);
}

int enfa_add_transition_from_symbol (ENFA * enfa, int from_state_id, char on_symbol, int to_state_id)
{
	enfa_add_transition (enfa, from_state_id, enfa_check_create_symbol (enfa, on_symbol), to_state_id);
}

void enfa_print_all (ENFA * enfa, FILE * str)
/* Just to show all info about the DFA. Useful only for debugging.. */
{
	int l,l1;
	fprintf (str, "Symbols in ENFA:\n");
	for (l = 0; l < enfa->_symbol_pool_count; l++)
	{
		fprintf (str, "   Symbol : %d , %c \n", enfa->_symbol_pool [l].id, enfa->_symbol_pool [l].symbol); 
	}
	fprintf (str, "States in ENFA:\n");
	for (l = 0; l < enfa->_state_pool_count; l++)
	{
		if (enfa->_state_pool [l].type == ENFA_STATE_FINAL)
			fprintf (str, " * ");
		else if (enfa->_state_pool [l].type == ENFA_STATE_INITIAL)
			fprintf (str, " + ");
		else
			fprintf (str, "   ");
		fprintf (str, "State : %d \n", enfa->_state_pool [l].id);
	}
	fprintf (str, "Transitions in ENFA:\n");
	for (l = 0; l < enfa->_transition_pool_count; l++)
	{
		fprintf (str, "   Transition : %d , (%d, %c) -> {", enfa->_state_pool [enfa->_transition_pool [l].from].id, enfa->_symbol_pool [enfa->_transition_pool [l].on].id, enfa->_symbol_pool [enfa->_transition_pool [l].on].symbol);
		for (l1 = 0; l1 < enfa->_transition_pool [l].to_count; l1++)
			fprintf(str, " %d", enfa->_transition_pool [l].to [l1]);
		fprintf(str, " }\n");
	}
}

int enfa_add_enfa ()
{
	if (_enfa_pool == NULL)
	{
		_enfa_pool = (ENFA *) malloc (sizeof (ENFA));
		_enfa_pool_count = 0;
	}
	else
	{
		_enfa_pool = (ENFA *) realloc (_enfa_pool, (_enfa_pool_count + 1) * sizeof (ENFA));
	}
	_enfa_pool [_enfa_pool_count].id = _enfa_id++;
	_enfa_pool [_enfa_pool_count]._initial_state_id = -1;
	_enfa_pool [_enfa_pool_count]._state_id = 0;
	_enfa_pool [_enfa_pool_count]._symbol_id = 0;
	_enfa_pool [_enfa_pool_count]._transition_id = 0;
	_enfa_pool [_enfa_pool_count]._state_pool = NULL;
	_enfa_pool [_enfa_pool_count]._symbol_pool = NULL;
	_enfa_pool [_enfa_pool_count]._transition_pool = NULL;
	_enfa_pool [_enfa_pool_count]._state_pool_count = 0;
	_enfa_pool [_enfa_pool_count]._symbol_pool_count = 0;
	_enfa_pool [_enfa_pool_count]._transition_pool_count = 0;
	//Insert a NULL symbol. The char passed doesn't matter.
	enfa_add_symbol (&_enfa_pool [_enfa_pool_count], 0);
	_enfa_pool_count++;	
	return (_enfa_id -1);
}

#endif
