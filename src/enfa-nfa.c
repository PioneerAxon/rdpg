#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<assert.h>
#include"enfa.c"
#include"nfa.c"
#include"nfa-dfa.c"

#ifndef __ENFA_NFA__
#define __ENFA_NFA__

//All structures are already in nfa-dfa.c.

void enfa_nfa_copy_symbols (ENFA * enfa, NFA * nfa)
{
	int l;
	for (l = 1; l < enfa->_symbol_pool_count; l++)
	{
		nfa_add_symbol (nfa, enfa->_symbol_pool [l].symbol);
	}
}


void enfa_nfa_cnvrt_print_tmp (FILE * str)
{
	State_LL * tmp;
	fprintf(str, "tmp = ");
	tmp = _tmp_list;
	while (tmp != NULL)
	{
		fprintf(str, "%d ", tmp->id);
		tmp = tmp->next;
	}
	fprintf(str, "\n");
}

void enfa_nfa_cnvrt_print_all(FILE * str)
{
	int l;
	State_LL * tmp;
	fprintf (str, "ENFA to NFA conversion chart : \n");
	for (l = 0; l < _states_pool_count;l++)
	{
		fprintf(str, "   NFA id = %d, ENFA id = { ", l, _states_pool [l].count);
		tmp = _states_pool [l].ll;
		while (tmp != NULL)
		{
			fprintf(str, "%d ", tmp->id);
			tmp = tmp->next;
		}
		fprintf(str, "}\n");
	}
}

int enfa_nfa_insert_ll ()
{
	if (_states_pool == NULL)
	{
		_states_pool = (State_Index *) malloc ((_states_pool_count + 1) * sizeof (State_Index));
		_states_pool_count = 0;
	}
	else
	{
		_states_pool = (State_Index *) realloc (_states_pool, (_states_pool_count + 1) * sizeof (State_Index));
	}
	_states_pool [_states_pool_count].count = 0;
	_states_pool [_states_pool_count].ll = NULL;
	return _states_pool_count++;
}

void enfa_nfa_insert_into_tmp (int val)
{
	State_LL * ptr;
	State_LL * new;
	new = (State_LL *) malloc (sizeof (State_LL));
	new->id = val;
	new->next = NULL;
	if (_tmp_list == NULL)
	{
		_tmp_list = new;
		_tmp_list_count = 0;
	}
	else
	{
		ptr = _tmp_list;
		if (ptr->id == val)
		{
			free (new);
			return;
		}
		if (ptr->id > val)
		{
			new->next = ptr;
			_tmp_list = new;
			_tmp_list_count++;
			return;
		}
		while (ptr->next != NULL)
		{
			if (ptr->next->id == val)
			{
				free (new);
				return;
			}
			if (ptr->next->id > val)
			{
				break;
			}
			ptr=ptr->next;
		}
		new->next = ptr->next;
		ptr->next = new;
	}
	_tmp_list_count++;
}

void enfa_nfa_insert_into_ll (int index, int val)
{
	State_LL * ptr;
	State_LL * new;
	if (index > _states_pool_count)
	{
		while (index != nfa_dfa_insert_ll ());
	}
	new = (State_LL *) malloc (sizeof (State_LL));
	new->id = val;
	new->next = NULL;
	if (_states_pool [index].ll == NULL)
	{
		_states_pool [index].ll = new;
	}
	else
	{
		ptr = _states_pool [index].ll;
		if (ptr->id == val)
		{
			free (new);
			return;
		}
		if (ptr->id > val)
		{
			new->next = ptr;
			_states_pool [index].ll = new;
			_states_pool [index].count++;
			return;
		}
		while (ptr->next != NULL)
		{
			if (ptr->next->id == val)
			{
				free (new);
				return;
			}
			if (ptr->next->id > val)
			{
				break;
			}
			ptr = ptr->next;
		}
		new->next = ptr->next;
		ptr->next = new;
	}
	_states_pool [index].count++;
}

int enfa_nfa_compare (State_LL * from1, State_LL * from2)
{
	while ( from1 != NULL && from2 != NULL)
	{
		if (from1->id != from2->id)
			return 0;
		from1 = from1->next;
		from2 = from2->next;
	}
	if (from1 == NULL && from2 == NULL)
		return 1;
	return 0;
}

int enfa_nfa_compare_add_state ()
{
	int l;
	int tmp = _states_pool_count;
	int new_ll;
	State_LL *prev;
	State_LL *next;
	for (l = 0; l < _states_pool_count; l++)
	{
		if (_states_pool [l].count == _tmp_list_count)
		{
			if (nfa_dfa_compare (_tmp_list, _states_pool [l].ll) == 1)
			{
				prev = _tmp_list;
				while (prev != NULL)
				{
					if ( prev->next == NULL)
					{
						free (prev);
						break;
					}
					next = prev->next;
					free (prev);
					prev = next;
				}
				_tmp_list = NULL;
				_tmp_list_count = 0;
				return l;
			}
		}
	}
	new_ll = nfa_dfa_insert_ll ();
	_states_pool [new_ll].ll = _tmp_list;
	_states_pool [new_ll].count = _tmp_list_count;
	_tmp_list = NULL;
	_tmp_list_count = 0;
	return (_states_pool_count - 1);
}

void enfa_nfa_e_closure (ENFA * enfa, int from_state)
{
	int l;
	int l1;
	State_LL * tmp;
	int added = 0;
	int old_size;
	if (from_state == -1)
	{
		added = 1;
	}
	else
	{
		enfa_nfa_insert_into_tmp (from_state);
		for (l = 0; l < enfa->_transition_pool_count; l++)
		{
			if (enfa->_transition_pool [l].from == from_state && enfa->_transition_pool [l].on == 0)
			{
				for (l1 = 0; l1 < enfa->_transition_pool [l].to_count; l1++)
				{
					old_size = _tmp_list_count;
					enfa_nfa_insert_into_tmp (enfa->_transition_pool [l].to [l1]);
					added += _tmp_list_count - old_size;
				}
			}
		}
	}
	if (added != 0)
	{
		tmp = _tmp_list;
		while (tmp != NULL)
		{
			enfa_nfa_e_closure (enfa, tmp->id);
			tmp = tmp->next;
		}
	}
}


int enfa_nfa_process_transition_from_states_on_symbol (ENFA * enfa, int convert_from_id, int on_symbol)
{
	int l1, l2, l3;
	int from_id;
	State_LL * ptr;
	ptr = _states_pool [convert_from_id].ll;
	for (l1 = 0; l1 < _states_pool [convert_from_id].count ;l1++)
	{
		for (l2 =0; l2 < enfa->_transition_pool_count; l2++)
		{
			if (enfa->_transition_pool [l2].from == ptr->id && enfa->_transition_pool [l2].on == on_symbol)
			{
				for (l3 = 0; l3 < enfa->_transition_pool [l2].to_count; l3++)
				{
					enfa_nfa_insert_into_tmp (enfa->_transition_pool [l2].to [l3]);
				}
			}
		}
		ptr = ptr->next;
	}
	enfa_nfa_e_closure (enfa, -1);
	if (_tmp_list_count == 0)
		return -1;
	return (enfa_nfa_compare_add_state ());
}

int enfa_nfa_check_is_final (ENFA * enfa, int states_id)
{
	int l;
	State_LL * tmp;
	tmp = _states_pool [states_id].ll;
	for (l = 0; l < _states_pool [states_id].count; l++)
	{
		if (enfa->_state_pool [tmp->id].type == ENFA_STATE_FINAL)
			return 1;
		tmp = tmp->next;
	}
	return 0;
}

void enfa_nfa_clear_ll (State_LL * from)
{
	State_LL * next;
	if (from == NULL)
		return;
	while (from != NULL)
	{
		next = from->next;
		free (from);
		from = next;
	}
}

void enfa_nfa_clear_all ()
{
	int l;
	if (_tmp_list != NULL)
	{
		nfa_dfa_clear_ll (_tmp_list);
		_tmp_list = 0;
		_tmp_list_count = 0;
	}
	for (l = 0; l < _states_pool_count; l++)
	{
		nfa_dfa_clear_ll (_states_pool [l].ll);
	}
	_states_pool = (State_Index *) realloc (_states_pool, 0);
	_states_pool_count = 0;
	_states_pool = NULL;
}



void enfa_nfa_convert (ENFA * from, NFA * to)
{
	enfa_nfa_copy_symbols (from, to);
	int l, l1, l2;
	int tmp;
	State_LL * ptr;
	enfa_nfa_e_closure (from, from->_initial_state_id);
	enfa_nfa_compare_add_state();
	for (l1 = 0; l1 < _states_pool_count; l1++)
	{
		for (l2 = 0; l2 < to->_symbol_pool_count; l2++)
		{
			tmp = enfa_nfa_process_transition_from_states_on_symbol (from, l1, l2 + 1);
			if (tmp != -1)
			{
				nfa_add_transition (to, l1, l2, tmp);
			}
		}
	}
	nfa_add_special_state_type (to, 0, NFA_STATE_INITIAL);
	for (l1 = 0; l1 < _states_pool_count; l1++)
	{
		if (enfa_nfa_check_is_final (from, l1))
		{
			nfa_add_special_state_type (to, l1, NFA_STATE_FINAL);
		}
	}
	enfa_nfa_cnvrt_print_all (stderr);
	enfa_nfa_clear_all();
}

#endif
